# 微服务

传统的微服务架构

服务之间的调用

1.RESTful As RPC: 调用方权限问题 (使用用户自己的token，操作不是用户触发的，或者查询全局数据)
2. RPC: 服务调用权限

调用方SDK:
1. RESTful As RPC: 自己封装SDK (API开放的(/vblog/api/v1)->SDK())
2. RPC: 常见的RPC框架都会自动生成SDK

关于内外接口拆分：RPC内部调用接口/Restful外部调用接口
https://www.mcube.top/docs/framework/api/

## 基于Restful 如何封装SDK
