package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"gitlab.com/vionysos/go15035-vic/skills/grpc/pb"
	"google.golang.org/grpc"
)

type HelloServiceServerImpl struct {
	pb.UnimplementedHelloServiceServer
}

func (i *HelloServiceServerImpl) Hello(ctx context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	resp := pb.NewHelloResponse()
	resp.Message = fmt.Sprintf("hello, %s", req.Name)
	return resp, nil
}
func main() {
	server := grpc.NewServer()
	pb.RegisterHelloServiceServer(server, &HelloServiceServerImpl{})

	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	server.Serve(lis)
}
