## GRPC



$ cd go15035-vic
$ protoc -I=. —-go_out=. -—go_opt=module="gitlab.com/vionysos/go15035-vic" skills/grpc/pb/*.proto

protoc -I=. --go_out=. --go_opt=module="gitlab.com/vionysos/go15035-vic" --go-grpc_out=. --go-grpc_opt=module="gitlab.com/vionysos/go15035-vic" skills/grpc/pb/*.proto


+ protoc-gen-go: 生成数据结构定义 struct
+ protoc-gen-go-grpc: 生成service相关 interface

## 客户端
接口：
```go
type HelloServiceClient interface {
	Hello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloResponse, error)
}



```

实现：
```go
type helloServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewHelloServiceClient(cc grpc.ClientConnInterface) HelloServiceClient {
	return &helloServiceClient{cc}
}

func (c *helloServiceClient) Hello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(HelloResponse)
	err := c.cc.Invoke(ctx, HelloService_Hello_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

```
```go
NewHelloServiceClient(grpc.conn).Hello(ctx, req)

```

## 服务端
服务端接口
```go
type HelloServiceServer interface {
	Hello(context.Context, *HelloRequest) (*HelloResponse, error)
	mustEmbedUnimplementedHelloServiceServer()
}

```

服务端实现
```go
type UnimplementedHelloServiceServer struct{}

func (UnimplementedHelloServiceServer) Hello(context.Context, *HelloRequest) (*HelloResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Hello not implemented")
}
func (UnimplementedHelloServiceServer) mustEmbedUnimplementedHelloServiceServer() {}
func (UnimplementedHelloServiceServer) testEmbeddedByValue()                      {}

```

需要继承UnimplentedHelloServiceServer对象，覆盖Hello方法, 实现直接的业务逻辑
## 注冊
```go
func RegisterHelloServiceServer(s grpc.ServiceRegistrar, srv HelloServiceServer) {
	// If the following call pancis, it indicates UnimplementedHelloServiceServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&HelloService_ServiceDesc, srv)
}

}
```