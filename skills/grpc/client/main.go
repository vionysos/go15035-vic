package main

import (
	"context"
	"fmt"

	"gitlab.com/vionysos/go15035-vic/skills/grpc/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {

	conn, err := grpc.NewClient("localhost:1234", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	resp, err := pb.NewHelloServiceClient(conn).Hello(context.TODO(), &pb.HelloRequest{Name: "bob"})
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}
