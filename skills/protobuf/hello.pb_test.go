package protobuf_test

import (
	"fmt"
	"log"
	"testing"

	"gitlab.com/vionysos/go15035-vic/skills/protobuf"
	"google.golang.org/protobuf/proto"
)

func TestProtobufMarshal(t *testing.T) {
	clientObj := &protobuf.String{Value: "hello proto3"}

	out, err := proto.Marshal(clientObj)
	if err != nil {
		log.Fatalln("Failed to encode obj:", err)
	}
	// 二进制编码
	fmt.Println("encode bytes: ", out)

	// 反序列化
	serverObj := &protobuf.String{}
	err = proto.Unmarshal(out, serverObj)
	if err != nil {
		log.Fatalln("Failed to decode obj: ", err)
	}
	fmt.Println("decode obj: ", serverObj)

}
