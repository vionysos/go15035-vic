package protobuf

import (
	"fmt"

	anypb "google.golang.org/protobuf/types/known/anypb"
)

func AnyTest() {
	a := Event{
		Type: EVENT_TYPE_ALERT,
	}
	req := &CreateUserRequest{}
	a.Data, _ = anypb.New(req)
	//

	req2 := &CreateUserRequest{}
	err := a.Data.UnmarshalTo(req2)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(a)
}
