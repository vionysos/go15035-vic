# Protobuf
protocol buffer


go 语言插件
```sh
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

vscode语法插件：vscode-proto3

```proto

syntax = "proto3";

package hello;
option go_package = "gitlab.com/vionysos/go15035-vic/skills/protobuf";

message String {
    string value = 1;
}
```
```go
type String struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value string `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
}
```
```sh
$ cd go15035-vic
$ protoc -I=. —-go_out=. -—go_opt=module="gitlab.com/vionysos/go15035-vic" skills/protobuf/hello.proto

自定义tag
安装protoc-go-inject-tag

加注释产生tag
运行protoc-go-inject-tag

 protoc-go-inject-tag -input="skills/protobuf/*.pb.go"
```