import {firstName, lastName, year} from './profile.js'
import { MYAPP } from './profile.js'
// import pkg from './profile.js'
// console.log(pkg.foo()                                                                                 )
console.log(firstName, lastName, year)
console.log(MYAPP.foo())
var x = 0;
var i;
for (i = 0; i < 10; i++) {
    x = x + i;
}
console.log(x)


var o = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};

var array01 = [1, 2, 3, 4]
for (var element of array01) {
    console.log(element)
}

for (var key of Object.keys(o)) {
    console.log(key);
}

array01.forEach((element, i, a) =>{
    console.log(element, i, a)
})


// 同时触发10个函数调用
// function myForEach(callbackfn) {
//     for (var element of array01) {
//         callbackfn(element)
//     }
// }
// myForEach((element)=> {
//     console.log(element)
// })

//Promise对象

//函数不能阻塞，需要立即返回
var QueryBlog = (cb) => {
    // 网络IO 后门异步任务
    // block io : resp--> array01 []
    cb(cb)
}

// go (block io)
// array01, err := vblog.QueryBlog()
QueryBlog((array01)=> {

})


// function callback() {
//     console.log('Done');


// }

// console.log('before setTimeout()');
// setTimeout(callback, 1000);
// console.log('after setTimeout()');



