export var firstName = "vic"
export var lastName = "chui"
export var year = 1973

// 唯一的全局变量MYAPP:
// export var MYAPP = {
//     name: "myapp",
//     version: 1.0,
//     foo: function () {
//         return "foo";
//     },
// };
// 默认导出 DEFAULT
export default {
    name: "myapp",
    version: 1.0,
    foo: function() {
        return "foo";
    },
};


function testResultCallbackFunc(success, failed) {
    var timeOut = Math.random() * 2;
    console.log('set timeout to: ' + timeOut + 'seconds.');

    setTimeout(function() {
        if(timeOut < 1) {
            console.log('call success()...');
            success('200 OK');
        } 
        else {
            console.log('call failed()...');
            failed('timeout in ' + timeOut + ' seconds.');
        }
    }, timeOut * 1000);
}

// testResultCallbackFunc(
//     (data) => {
//         console.log(data)
//     },
//     (err) => {
//         console.log(err)
//     }
// )

// console.log("testResultCallbackFunc call 完成")


//采用Promise对象封装，封装成Promise, 发明一个Promise语法
// 成功调用 then (fn)
// 失败调用 catch(fn)
// 写法上更优雅, 同时也统一了语法
// var p1 = new Promise(testResultCallbackFunc)
// p1.then((resp) => {
//     console.log(resp)
//         p2.then((v) => {
//             p3.then((v) => {

//             })
//         }).cache((e) => {
//             p4.then((v) => {

//             })
//         })
// }).catch((err) => {
//     console.log(err)
// })


// 生命这个函数 在executor中允许
// 由executor的 await 函数获取 异步对象的返回结果
// 从此告别回调地狱，本质还是回调执行，有Excutor 等待并通知结果
// async + await 使用同步的写法来写异步程序
async function testWithAsync() {
    var p1 = new Promise(testResultCallbackFunc)
    var resp1 = await p1
    var p2 = new Promise(testResultCallbackFunc)
    var resp2 = await p2
    var p3 = new Promise(testResultCallbackFunc)
    var resp3 = await p3
}


testWithAsync()
