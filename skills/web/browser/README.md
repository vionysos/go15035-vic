## 浏览器

[浏览器] (https://gitee.com/infraboard/go-course/blob/master/day19/browser.md )


## Ajax
```sh
fetch('http://127.0.0.1:8080/vblog/api/v1/blogs')
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.error('请求失败', error));
```

服务端怎么添加CORS