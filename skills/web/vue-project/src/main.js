import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
import TestComponent from './components/TestComponent.vue'
app.component('TestComponent', TestComponent)
app.mount('#app')
