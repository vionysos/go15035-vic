package service

type Service interface {
	Hello(string, *string) error
}

type HelloRequest struct {
	Name string
}

type HelloResponse struct {
	Message string
}
