package conf

import (
	"os"

	"github.com/caarlos0/env"
	"gopkg.in/yaml.v3"
)

// 配置加载
// file/env/... --> Config
// 全局一份

var config *Config

func C() *Config {
	if config == nil {
		config = Default()
	}

	return config
}

// 加载配置

func LoadConfigFromYaml(configPath string) error {
	content, err := os.ReadFile(configPath)
	if err != nil {
		return err
	}
	config = C()

	return yaml.Unmarshal(content, config)
}

// "github.com/caarlos0/env/v6"
func LoadConfigFromEnv() error {
	config = C()
	//config = Default()
	//config.MySQL.DB = os.Getenv("MYSQL_DB")
	return env.Parse(config)
}
