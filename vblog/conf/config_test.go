package conf_test

import (
	"os"
	"testing"

	"gitlab.com/vionysos/go15035-vic/vblog/conf"
)

func TestToYAML(t *testing.T) {
	t.Log(conf.Default().ToYAML())
}

func TestToLoadFromEnv(t *testing.T) {
	os.Setenv("DATASOURCE_USERNAME", "env test")
	err := conf.LoadConfigFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().ToYAML())
}
func TestToLoadFromYAML(t *testing.T) {
	err := conf.LoadConfigFromYaml("./application.yml")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(conf.C().ToYAML())
}
