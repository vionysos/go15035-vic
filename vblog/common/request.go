package common

type UPDATE_MODE int

const (
	//全量更新模式
	UPDATE_MODE_PUT = iota
	// 部份更新模式
	UPDATE_MODE_PATCH
)
