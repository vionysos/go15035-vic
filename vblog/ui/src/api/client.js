import { Message } from '@arco-design/web-vue'
import axios from 'axios'

var client = axios.create({
  baseURL: '',
  timeout: 5000
})

client.interceptors.response.use(
  (value) => {
    return value.data
  },
  (err) => {
    console.log(err)
    var msg = err.message
    var code = 0
    if (err.response && err.response.data) {
      msg = err.response.data.message
      code = err.response.data.code
    }
    switch (code) {
      case 50000:
        location.assign('/login')
        break
      case 50002:
        location.assign('/login')
        break
      case 50003:
          location.assign('/login')
          break
    }
    Message.error(msg)
    return Promise.reject(err)
  }
)
export default client
