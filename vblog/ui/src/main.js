import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import ArcoVueIcon from '@arco-design/web-vue/es/icon'
const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(ArcoVueIcon)
import ArcoVue from '@arco-design/web-vue'
// import '@arco-design/web-vue/dist/arco.css'
import '@arco-themes/vue-jzg-color/index.less'
import '@/assets/arco.css'
app.use(ArcoVue)

app.mount('#app')
