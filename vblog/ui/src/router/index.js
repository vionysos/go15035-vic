import { createRouter, createWebHistory } from 'vue-router'
import app from '@/stores/app'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'LoginPage',
      // 没有嵌套，这个就是最终的页面
      // import
      // 惰性加载：
      component: () => import('../views/login/LoginPage.vue')
    },
    {
      path: '/backend',
      name: 'BackendLayoutPage',
      component: () => import('@/views/backend/BackendLayout.vue'),
      redirect: { name: 'BackendBlogsList' },
      beforeEnter: () => {
        if (!app.value.token) {
          return { name: 'LoginPage' }
        }
      },
      children: [
        {
          // backend/vblogs
          path: 'blogs/list',
          name: 'BackendBlogsList',
          component: () => import('@/views/backend/blogs/ListPage.vue')
        },

        {
          // backend/vblogs
          path: 'blogs/edit',
          name: 'BackendBlogsEdit',
          component: () => import('@/views/backend/blogs/EditPage.vue')
        },
        {
          // backend/vblogs
          path: 'comments/list',
          name: 'BackendCommentList',
          component: () => import('@/views/backend/comment/ListPage.vue')
        },
        {
          // backend/vblogs
          path: 'tags/list',
          name: 'BackendTagList',
          component: () => import('@/views/backend/tag/ListPage.vue')
        }
      ]
    },
    {
      path: '/frontend',
      name: 'FrontendLayout',
      component: () => import('../views/frontend/FrontendLayout.vue'),
      redirect: { name: 'FrontendBlogList' },
      children: [
        {
          //backend/vblogs
          path: 'blogs/list',
          name: 'FrontendBlogList',
          component: () => import('../views/frontend/blogs/ListPage.vue')
        },
        {
          //backend/vblogs
          path: 'blogs/detail',
          name: 'FrontendBlogDetail',
          component: () => import('../views/frontend/blogs/DetailPage.vue')
        }
      ]
    },
    // 匹配前面所有没有被名字的路由，都指向404页面
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: () => import('@/views/common/NotFound.vue')
    }
  ]
})

export default router
