# ui

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

[前端框架搭建](https://gitee.com/infraboard/go-course/blob/master/day21/vblog-base.md)

### 清理模版

只留下App.vue这个root视图

### 安装UI插件

[UI组件](https://gitee.com/infraboard/go-course/blob/master/day20/vue3-ui.md)

### UI 原型

[UI原型](https://gitee.com/infraboard/go-course/blob/master/day21/vblog-base.md)

### 登录页面开发

使用vite config.js来解决跨域访问的问题

client.js

### 后台管理页面开发

### 后台框架

### 后台文章管理

### 前台展示页面开发
