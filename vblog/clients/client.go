package clients

import (
	"context"
	"fmt"

	"github.com/go-resty/resty/v2"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/blog"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
)

func NewClient(address string) Client {
	c := resty.New()
	c.BaseURL = address
	c.Header.Add("Content-type", "application/json")
	c.OnAfterResponse(func(c *resty.Client, r *resty.Response) error {
		if r.StatusCode()/100 != 2 {
			return fmt.Errorf("status code: %d", r.StatusCode())
		}
		return nil
	})
	return &Client{
		c: c,
	}

}

func (c *Client) Debug(v bool) {
	c.c.Debug = v
}

type Client struct {
	c *resty.Client
}

func (c *Client) Auth(ctx context.Context, req *token.IssueTokenRequest) error {
	tk := token.DefaultToken()
	_, err := c.c.R().SetContext(context.Background()).SetBody(token.NewIssueTokenRequest(username, password)).SetResult(tk).Post("/vblog/api/v1/tokens")
	if err != nil {
		return err
	}
	c.c.SetAuthToken(tk.AccessToken)
	return nil

	// 	url := "http://127.0.0.1:8080/vblog/api/v1/tokens"
	// 	method := "POST"

	// 	payload := strings.NewReader(`{
	// 	  "username": "author",
	// 	  "password": "123456"
	//   }`)

	// 	client := &http.Client{}
	// 	req, err := http.NewRequest(method, url, payload)

	// 	if err != nil {
	// 		fmt.Println(err)
	// 		return
	// 	}
	// 	req.Header.Add("Content-Type", "application/json")

	// 	res, err := client.Do(req)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 		return
	// 	}
	// 	defer res.Body.Close()

	// body, err := ioutil.ReadAll(res.Body)
	//
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//
	// fmt.Println(string(body))
}

func (c *Client) QueryBlog(ctx context.Context, req *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	set := blog.NewBlogSet()
	_, err := c.c.R().SetContext(ctx).SetResult(set).Get("/vblog/api/v1/blogs")
	if err != nil {
		return nil, err
	}
	return set, nil
}
