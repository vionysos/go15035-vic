package clients_test

import (
	"context"
	"testing"

	"gitlab.com/vionysos/go15035-vic/vblog/apps/blog"
	"gitlab.com/vionysos/go15035-vic/vblog/clients"
)

// clients: QueryBlog
// impl: QueryBlog
// Restful 调用 用作 rpc: json on HTTP
// client: 就是vblog这个服务的SDK 给其他服务使用
func TestQueryUser(t *testing.T) {

	client := clients.NewClient("http://127.0.0.1:8080")
	if err := client.Auth("admin", "123456"); err != nil {
		t.Fatal(err)
	}

	client.Debug(false)

	set.err := client.QueryBlog(context.TODO(), blog.NewQueryBlogRequest())
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
