package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/user"
	"gitlab.com/vionysos/go15035-vic/vblog/response"
)

// 这是一个需要有参数的中间件：Require("auditor")
// 通过一个函数返回一个中间件：gin HandleFunc
// 这中间件是加载在认证中间件之后的
func Require(requireRoles ...user.Role) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		if v, ok := ctx.Get(token.GIN_TOKEN_KEY_NAME); ok {
			for i := range requiredRoles {
				requiredRole := requiredRoles[i]
				if v.(*token.Token).Role == requiredRole {
					ctx.Next()
					return
				}
			}
		}
		response.Failed(token.ErrPermissionDeny, ctx)
		ctx.Abort()
	}

}
