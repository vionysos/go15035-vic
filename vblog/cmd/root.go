package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	initCmd "gitlab.com/vionysos/go15035-vic/vblog/cmd/init" // Import the package that defines initCmd
	"gitlab.com/vionysos/go15035-vic/vblog/cmd/start"
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
)

var (
	configPath string
)

var RootCmd = &cobra.Command{
	Use:   "vblog",
	Short: "vblog Service",
	Run: func(cmd *cobra.Command, args []string) {

		// vblog version
		// v0.0.1
		if len(args) > 0 {
			if args[0] == "version" {
				fmt.Println("v0.0.1")
			}
		} else {
			cmd.Help()
		}
	},
}

func Execute() error {
	// 初始化需要执行的逻辑
	cobra.OnInitialize(func() {
		// 1. 加载配置
		cobra.CheckErr(conf.LoadConfigFromYaml(configPath))
		// 2. 初始化IoC
		cobra.CheckErr(ioc.Controller.Init())
		// 3. 初始化Api
		cobra.CheckErr(ioc.Api.Init())
	})

	return RootCmd.Execute()

}

func init() {
	// config
	RootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "etc/application.yaml", "the service config file")
	// Root-->init
	RootCmd.AddCommand(initCmd.Cmd) // Use initCmd to add the command to RootCmd
	//Root-->start
	RootCmd.AddCommand(start.Cmd)

}
