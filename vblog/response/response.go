package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vionysos/go15035-vic/vblog/exception"
)

func Success(data any, c *gin.Context) {
	return c.JSON(http.StatusOK, data)

}

func Failed(err error, c *gin.Context) {
	// 非200 状态, 接口报错, 返回内容：ApiException对象
	httpCode := http.StatusInternalServerError
	if v, ok := err.(*exception.ApiException); ok {
		if v.HttpCode != 0 {
			httpCode = v.HttpCode
		}
	} else {
		err = exception.ErrServerInternal(err.Error())
	}
	c.JSON(httpCode, err)
	c.Abort()
}
