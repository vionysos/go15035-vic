package test

import (
	"github.com/spf13/cobra"
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps"
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
)

func DevelopmentSetup() {
	if err := conf.LoadConfigFromEnv(); err != nil {
		panic(err)
	}
	// 2.初始化 IoC
	cobra.CheckErr(ioc.Controller.Init())
	// 3.初始化 Api
	cobra.CheckErr(ioc.Api.Init())
}
