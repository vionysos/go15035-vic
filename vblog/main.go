package main

import "gitlab.com/vionysos/go15035-vic/vblog/cmd"

func main() {

	if err := cmd.Execute(); err != nil {
		panic(err)

	}
}
