# IOC (依赖倒置) : 对象依赖管理

## 现在

```go
// User BO
	userServiceImpl := user.NewUserServiceImpl()
	// Token BO
	tokenServiceImpl := token.NewTokenServiceImpl(userServiceImpl)
	// token 模块子路有
	tokenApiHandler := api.NewTokenApiHandler(tokenServiceImpl)


```

在程序启动的时候(main), 收到传递依赖，main组装流程异常复杂, 如果有2个模块(20 serviceImpl, 20 API对象)，main逻辑很复杂

## IOC

让软件工程规模化, 很好的模块化管理

为了解决对象之间的耦合度过高的问题，软件专家Michael Mattson 1996年提出了IOC提论, 
用来实现对象之间的"解耦", 目前这个理论已经被成功地应用到实践当中

IOC理论提出的观点大体是这样的：借助于"第三方"实现具有依赖关系的对象之间的解耦
IOC是Inversion of Control 的缩写，多数书籍翻译成"控制反转", 使用


### IOC Container

系统内容所有业务对象(BO) 的一个托儿所，这样业务对象






参数不需要主动传递，对象自己主动获取
```go
func NewTokenSErviceImpl() *TokenServiceImpl {

    return  &TokenServiceImpl {
        db: conf.C().MySQL.GetDB(),
        user: ioc.GetObject("user").(user.Service),
    }
}

func init() {
    ioc.Controller().Register("token", &TokenServiceImpl{})
}



```

注册User模块
```go

```



市面上没有Ioc Container的具体实现：

+ [dig] (https://www.bilibili.com/read/cv24867421/)
+ golang-ioc
+ mcube ioc

基本的使用样例：
```go
func TestRegistry(t *testing.T) {
	ioc.Container.Registry("user", user.UserServiceImpl{})
	t.Log(ioc.Controller.Get("user"))
}
```

### 业务的具体实现Controller 托管到IoC
1. 托管
2. 托管 TokenServiceImpl 到IoC
3. TokenServiceImpl 通过
4.
5. 程序启动的时候注册业务，并初始化IoC
6. 之前New的逻辑IoC已经帮忙完成，所有依赖通过IoC获取

### 