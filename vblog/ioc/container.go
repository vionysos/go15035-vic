package ioc

var Controller Container = &MapContainer{
	name: "controller",
	// [], 自定义结构
	storage: make(map[string]Object),
}

// API 所有的对外接口对象都放这里
var Api Container = &MapContainer{

	name: "api",
	// [], 自定义结构
	storage: make(map[string]Object),
}
