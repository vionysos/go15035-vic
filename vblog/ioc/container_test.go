package ioc_test

import (
	"testing"

	user "gitlab.com/vionysos/go15035-vic/vblog/apps/user/impl"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
)

func TestRegistry(t *testing.T) {
	ioc.Controller.Registry("user", user.UserServiceImpl{})
	t.Logf("%p", ioc.Controller.Get("user"))
}
