package ioc

import (
	"fmt"
)

type MapContainer struct {
	name    string
	storage map[string]Object
}

// 注册对象

func (c *MapContainer) Registry(name string, obj Object) {

	c.storage[name] = obj
}

func (c *MapContainer) Get(name string) any {
	return c.storage[name]
}

func (c *MapContainer) Init() error {

	for k, v := range c.storage {
		if err := v.Init(); err != nil {
			return fmt.Errorf("%s init error, %s", k, err)
		}
		fmt.Printf("[%s] %s init success", c.name, k)
	}
	return nil

}
