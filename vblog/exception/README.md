## API 异常定义(自定义API异常)
+ 用户名或者密码不正确
+ 用户不存在
+ Token过期

![alt text](image.png)
这些场景，为什么要定义业务异常

## 现状

```go
return fmt.Error("用户名或密码不正确")
```
只有一个message


## 定义的业务异常

```go
// 用于描述业务异常
type ApiException struct {
	// 业务异常的编码，50001 表示Token过期
	Code int `json:"code"`
	// 异常描述信息
	Message string `json:"message"`
}
```


```go
IssueToken(context.Context, *IssueTokenRequest)(*Token, error)
```


我们需要扩展error
```go
// The error built-in interface type is the conventional interface for
// representing an error condition, with the nil value representing no error.

type error interface {
    Error() string
}
```


## 使用

```go
func TestException(t *testing.T) {
    err := CheckIsError()


    t.Log (err)

    if v, ok := err.(*exception.ApiException); ok {
        t.Log(v.Code)
        t.Log(v.String())



    }


    dj, _ := json.MarshalIndent(err, "", "  ")
    t.Log(string(dj))
}

```
```go
```