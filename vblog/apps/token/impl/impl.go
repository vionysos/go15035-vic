package impl

import (
	"context"
	"fmt"
	"go/token"

	"gitlab.com/go-course-project/go15/vblog/apps/token"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/user"
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/exception"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
	"gorm.io/gorm"
)

// func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {

// 	return &TokenServiceImpl{
// 		db:   conf.C().MySQL.GetDB(),
// 		user: userServiceImpl,
// 	}
// }

// import _ ----> init方法来注册 包里面的核心对象
func init() {
	ioc.Controller.Register(token.AppName, &TokenServiceImpl{})
}

type TokenServiceImpl struct {
	// cb conn 共享对象
	// mysql host port ....
	db *gorm.DB
	// 依赖用户服务
	user user.Service
}

func (i *TokenServiceImpl) Init() error {
	//return &TokenServiceImpl{
	// 		db:   conf.C().MySQL.GetDB(),
	// 		user: userServiceImpl,
	// 	}

	i.db = conf.C().MySQL.GetDB()
	// 获取对象 Controller.Get(user.AppName)
	// 断言对象实现了user.Service
	i.user = ioc.Controller.Get(user.AppName).(user.Service)
	return nil

}

func (i *TokenServiceImpl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	// 1.查询用户对象
	// i.QueryUser 是调用的user.Service的接口吗？
	queryUser := user.NewQueryUserRequest()
	queryUser.Username = in.Username

	us, err := i.user.QueryUser(ctx, queryUser)
	if err != nil {
		return nil, err
	}
	if len(us.Items) == 0 {
		// 安全：避免被爆破
		return nil, token.ErrAuthFailed
	}
	// 2. 比对用户密码
	u := us.Item[0]
	if err := u.checkPassword(in.Password); err != nil {
		return nil, token.ErrAuthFailed
	}

	// 3.颁发一个令牌 (token)
	tk := token.NewToken(u)
	// 4.把令牌存储在数据库里面

	if err := i.db.WithContext(ctx).Create(tk).Error; err != nil {
		return nil, exception.ErrServerInternal("保存保存, %s", err)
	}
	// 5. 返回令牌

	return tk, nil
}

func (i *TokenServiceImpl) RevokeToken(
	ctx context.Context,
	in *token.RevokeTokenRequest) (*token.Token, error) {
	// ErrServerInternal 没有使用这个异常
	// 后面可以用中间件来统一处理：非ApiException -->ErrServerInternal
	// 直接删除数据库里面存储的Token
	// 1. 查询出Token, Where AccessToken来查询

	// 查询一个
	tk := token.DefaultToken()
	err := i.db.
		WithContext(ctx).
		Where("access_token = ?", in.AccessToken).
		First(tk).
		Error
	if err == gorm.ErrRecordNotFound {
		return nil, exception.ErrNotFound("Token没找到")
	}

	if tk.RefreshToken != in.RefreshToken {
		return nil, fmt.Errorf("RefreshToken不正确")
	}

	// DELETE FROM `tokens` WHERE access_token = 'cpdutg197i6bgqoevclg'
	err = i.db.
		WithContext(ctx).
		Where("access_token = ?", in.AccessToken).
		Delete(token.Token{}).
		Error
	if err != nil {
		return nil, err
	}
	return tk, nil

}

func (i *TokenServiceImpl) ValidateToken(
	ctx context.Context,
	in *token.ValidateTokenRequest) (*token.Token, error) {
	// ErrServerInternal 没有使用这个异常
	// 后面可以用中间件来统一处理：非ApiException -->ErrServerInternal
	// 直接删除数据库里面存储的Token
	// 1. 查询出Token, Where AccessToken来查询

	// 查询一个
	tk := token.DefaultToken()
	err := i.db.
		WithContext(ctx).
		Where("access_token = ?", in.AccessToken).
		First(tk).
		Error
	if err == gorm.ErrRecordNotFound {
		return nil, exception.ErrNotFound("Token未找到")

	}
	if err != nil {
		return nil, exception.ErrServerInternal("查询报错, %s", err)
	}

	// 2. 判断Token是否过去 1。先判断RefreshToken有没有过期 2. AccessToken有没有过期
	if err := tk.RefreshTokenIsExpired(); err != nil {
		return nil, err
	}
	if err := tk.AccessTokenIsExpired(); err != nil {
		return nil, err
	}

	// 3. Token合法 1. 是我颁发 2. 没有过期

	// 返回查询到的Token
	// 4. 补充用户角色信息
	queryUserReq := user.NewQueryUserRequest()
	queryUserReq.Username = tk.UserName
	us, err := i.user.QueryUser(ctx, queryUserReq)
	if err != nil {
		return nil, err
	}
	if len(us.Items) == 0 {
		return nil, fmt.Errorf("token user not found")

	}
	tk.Role = us.Items[0].Role
	return tk, nil
}
