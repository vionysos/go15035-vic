package impl_test

import (
	"context"
	"testing"

	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
)

var (
	serviceImpl token.Service
	ctx         = context.Background()
)

func init() {
	// 使用构造函数
	//serviceImpl = impl.NewTokenServiceImpl(user.NewUserServiceImpl())
	// 去IoC中获取 被测试的业务对象
	serviceImpl = ioc.Controller.Get(token.AppName).(token.Service)
}
func TestIssueToken(t *testing.T) {
	req := token.NewIssueTokenRequest("admin", "123456")
	tk, err := serviceImpl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestRevokeToken(t *testing.T) {

	req := token.NewRevokeTokenRequest("cpduaoh97i68rbsc3mqg", "cpduaoh97i68rbsc3mqg")
	tk, err := serviceImpl.RevokeToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidateToken(t *testing.T) {

	req := token.NewValidateTokenRequest("cpduaoh97i68rbsc3mqg")
	tk, err := serviceImpl.ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}
