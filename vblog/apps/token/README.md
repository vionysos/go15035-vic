#令牌管理模块
## 关于单元测试

1. 被测试文件：文件名_test.go
2. 被测试包：包名_test token --> token_test
3. 被测试函数：
     + 测试函数名称： Test被测试函睥的名称
     + 测试函数签名： (t *test.Testing)


### Vscode 单元测试配置
vscode 调用了一个命令：
```sh
Running tool: /usr/local/go/bin/go test -timeout 300s -run ^TestTokenString$ gitlab.com/vionysos/go15035-vic/vblog/apps/token -v -count=1
```
编辑 vscode编程 go test 参数
+ -v: 打印Debug时候的日志
+ -count = 1: 至少执行一次，如果你的单元测试没有修改，单元测试的结果会被缓存







### Debug Test





