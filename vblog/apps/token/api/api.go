package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/response"
)

// func NewTokenApiHandler(tokenServiceImpl token.Service) *TokenApiHandler {

// 	return &TokenApiHandler{
// 		//token: tokenServiceImpl,
// 		token: ioc.Controller.Get(token.AppName).(token.Service),
// 	}
// }

func init() {

	ioc.Api.Register(token.AppName, &TokenApiHandler{})
}

type TokenApiHandler struct {
	token token.Service
}

func (h *TokenApiHandler) Init() error {

	h.token = ioc.Controller.Get(token.AppName).(token.Service)
	//把自己注册到Root Router
	// /vblog/v1/前置
	subRouter := conf.C().Application.GinRootRouter().Group("tokens")
	h.Registry(subRouter)
	return nil
}

// 把自己的路由信息注册给Gin Root Router
// Gin Engine --> 子Router gin.IRouter
// 每个业务模块，有每个业务的子路由
func (h *TokenApiHandler) Registry(appRouter gin.IRouter) {
	// r := gin.Default()
	// // api/v1 --> root group
	// // RouterGroup --> gin.IRouter
	// r.Group("api").Group("v1")
	appRouter.POST("/", h.Login)
	appRouter.DELETE("/", h.Logout)
}

func (h *TokenApiHandler) Login(c *gin.Context) {
	// 1. 获取HTTP请求
	req := token.NewIssueTokenRequest("", "")
	if err := c.BindJSON(req); err != nil {
		response.Failed(err, c)
		return
	}
	// 2. 业务处理
	tk, err := h.token.IssueToken(c.Request.Context(), req)

	if err != nil {
		response.Failed(err, c)
		return
	}

	// 3. 返回结果
	// c.JSON()
	// c.Writer.Write()
	// 通过SetCookie 发Cookie 设置到浏览器中，让浏览器发送请求的时候都代码
	// 你使用的http客户端 支不支持 Set-Cookie头
	c.SetCookie(
		token.COOKIE_TOKEN_KEY,
		tk.AccessToken,
		tk.RefreshTokenExpiredAt,
		"/",
		conf.C().Application.Domain,
		false,
		true,
	)
	response.Success(tk, c)
}

func (h *TokenApiHandler) Logout(c *gin.Context) {
	// 1. 获取HTTP 请求 <--> IssueTokenRequest
	// DELETE方法 一般情况不带Body
	// 敏感信息放Header或者Body
	ak, err := c.Cookie(token.COOKIE_TOKEN_KEY)
	if err != nil {
		response.Failed(err, c)
		return
	}

	rt := c.GetHeader(token.REFRESH_HEADER_KEY)
	req := token.NewRevokeTokenRequest(c.Request.Context(), req)
	if err != nil {
		response.Failed(err, c)
		return
	}
	response.Success(tk, c)

}
