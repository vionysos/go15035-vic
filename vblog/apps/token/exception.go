package token

import (
	"net/http"

	"gitlab.com/vionysos/go15035-vic/vblog/exception"
)

var (
	ErrUnauthorized        = exception.NewApiException(500000, "请登录").WithHttpCode(http.StatusUnauthorized)
	ErrAuthFailed          = exception.NewApiException(500001, "用户名或密码错误").WithHttpCode(http.StatusUnauthorized)
	ErrAccessTokenExpired  = exception.NewApiException(500002, "Access令牌过期")
	ErrRefreshTokenExpired = exception.NewApiException(500003, "Refresh令牌过期")
	ErrPermissionDeny      = exception.NewApiException(500004, "当前角色无权限访问该接口").WithHttpCode(http.StatusForbidden)
)
