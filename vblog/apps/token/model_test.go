package token_test

import (
	"testing"
	"time"

	"gitlab.com/vionysos/go15035-vic/vblog/apps/token"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/user"
)

func TestTokenStr(t *testing.T) {
	tk := token.Token{
		UserId: 1,
		Role:   user.ROLE_ADMIN,
	}
	t.Log(tk.String())
}
func TestTokenExpired(t *testing.T) {
	now := time.Now().Unix()

	tk := token.Token{
		UserId:               1,
		Role:                 user.ROLE_ADMIN,
		AccessTokenExpiredAt: 1,
		CreatedAt:            now,
	}
	t.Log(tk.AccessTokenIsExpired())

}
