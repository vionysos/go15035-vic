package user

import (
	"encoding/json"
	"fmt"

	"gitlab.com/vionysos/go15035-vic/vblog/common"
	"golang.org/x/crypto/bcrypt"
)

//存放的是PO ，需要入库的对象

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  ROLE_VISITOR,
		Label: map[string]string{},
	}
}

type CreateUserRequest struct {
	Username string `json:"username" validate:"required" gorm:"column:username"`
	Password string `json:"password" validate:"required" gorm:"column:password"`
	Role     Role   `json:"role" gorm:"column:role"`
	// https://gorm.io/docs/serializer.html
	// 用户标签 {"group": "a"}  --json-> "{}"
	// 专门设计： label id key value
	Label map[string]string `json:"label" gorm:"column:label;serializer:json"`
}

// validator
// 1. 初始化一个校验器
func (req *CreateUserRequest) Validate() error {

	if req.Username == "" {
		return fmt.Errorf("用戶名必填")
	}
	return nil
}
func (req *CreateUserRequest) HashPassword() error {
	cryptoPass, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	req.Password = string(cryptoPass)
	return nil

}

func (req *CreateUserRequest) CheckPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(req.Password), []byte(password))
}

type Meta struct {
	Id        int   `json:"id" gorm:"column:id"`
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
}

func NewUser(req *CreateUserRequest) *User {
	return &User{
		Meta:              common.NewMeta(),
		CreateUserRequest: req,
	}
}

type User struct {
	// // 用戶id
	// Id int `json:"id" gorm:"column:id"`
	// // 创建时间，时间戳 10位，秒
	// CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// // 更新时间，时间戳 10位 秒
	// UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
	// // 用户参数
	*common.Meta
	*CreateUserRequest
}

func (req *User) String() string {
	dj, _ := json.MarshalIndent(req, "", "	")
	return string(dj)
}

func (req *User) TableName() string {
	return "users"
}
func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

type UserSet struct {
	// 总共有多个(分页, 数据库面面总共)
	Total int64   `json:"total"`
	Items []*User `json:"items"`
}
