package user

import (
	"context"

	"gitlab.com/vionysos/go15035-vic/vblog/common"
)

const (
	AppName = "user"
)

// user.Service
// 用户管理接口

type Service interface {
	// 用户创建
	// 1. 用户取消了请求怎么办?
	// 2. 后面要做Trace, Trace ID怎么传递进来
	// 3. 多个接口，需要做事务 (Session), 事务对象怎么传递进来
	// 4. CreateUser(username, password string)
	CreateUser(context.Context, *CreateUserRequest) (*User, error)

	// 用户查询
	QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		PageRequest: common.NewPageRequest(),
	}
}

type QueryUserRequest struct {
	Username string

	*common.PageRequest
}
