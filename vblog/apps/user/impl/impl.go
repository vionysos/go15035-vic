package impl

import (
	"context"

	"gitlab.com/vionysos/go15035-vic/vblog/apps/user"
	"gitlab.com/vionysos/go15035-vic/vblog/common"
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"

	"gorm.io/gorm"
)

//	func NewUserServiceImpl() *UserServiceImpl {
//		//db = create conn
//		// 每个业务对象，都可能依赖到 数据库
//		// 获取一个全的 mysql 连接池对象
//		// 程序启动的时候一定要加载配置
//		return &UserServiceImpl{
//			db: conf.C().MySQL.GetDB(),
//		}
//	}
func init() {

	ioc.Controller.Registry(user.AppName, &UserServiceImpl{})
}

type UserServiceImpl struct {
	// 每个业务对象，都可能依赖到数据库
	// db = create conn
	// 全局的MySQL
	db *gorm.DB
}

func (i *UserServiceImpl) Init() error {
	// 获取对象 Controller.Get()
	i.db = conf.C().MySQL.GetDB()
	return nil
}

func (i *UserServiceImpl) CreateUser(
	ctx context.Context,
	in *user.CreateUserRequest) (
	*user.User, error) {
	// 1. 校验请求的合法性
	if err := common.Validate(in); err != nil {
		return nil, err
	}
	if err := in.HashPassword(); err != nil {
		return nil, err
	}

	// 2. 创建user对象(资源)
	ins := user.NewUser(in)
	// 3. user 对象保持入库
	/*
		读取数据库配置
		获取数据库连接
		操作连接 保证数据
	*/
	//INSERT INTO `users` (`created_at`,`updated_at`,`username`,`password`,`role`,`label`) VALUES (1717281667,1717281667,'admin','123456',0,'{}')
	if err := i.db.Save(ins).Error; err != nil {
		return nil, err
	}
	// 4. 返回保持后的user对象
	return ins, nil

}
func (i *UserServiceImpl) QueryUser(
	ctx context.Context,
	in *user.QueryUserRequest) (
	*user.UserSet, error) {
	set := user.NewUserSet()

	// 	构造一个查询语句，TableName() select
	// WithContext
	query := i.db.Model(&user.User{}).WithContext(ctx)
	// Where where username -?
	// SELECT * FROM `users` WHERE username = `admin` LIMIT 10
	if in.Username != "" {
		query = query.Where("username = ?", in.Username)

	}

	// 怎么查询Total, 需要把过滤条件：username, key
	// 查询Total 时能不能把分页参数带上
	// select COUNT(*) from xxx limit 10
	// select COUNT(*) from xxx
	// 不能携带分页参数
	if err := query.Count(&set.Total).Error; err != nil {
		return nil, err
	}
	// LIMIT ?, ?
	// SELECT * FROM `users` LIMIT 10
	if err := query.
		Offset(in.Offset()).
		Limit(in.PageSize).
		Find(&set.Items).Error; err != nil {
		return nil, err
	}
	return set, nil

}
