package apps

import (
	// 业务对象注册
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps/blog/api"
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps/blog/impl"
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps/token/api"
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps/token/impl"
	_ "gitlab.com/vionysos/go15035-vic/vblog/apps/user/impl"
)
