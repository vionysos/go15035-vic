package blog

type Status int

const (
	STATUS_DRAFT = iota
	STATUS_PUBLISH
)
