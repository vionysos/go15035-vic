# 博客文章管理



## 定义业务


3.
4. 编写单元测试
## 实现业务



## 实现接口
- 博客创建接口 CreateBlog: POST /vblog/api/v1/blogs
- 博客列表接口 Blog: GET /vblog/api/v1/blogs?page_size=10&page_number=1&...
- 博客全量修改接口 Blog: PUT /vblog/api/v1/blogs/{id}
- 博客增量修改接口 Blog: PATCH /vblog/api/v1/blogs/{id}
- 博客刪除接口 Blog: DELETE /vblog/api/v1/blogs/{id}

## 托管给IoC