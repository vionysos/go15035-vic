package impl_test

import (
	"context"

	_ "gitlab.com/vionysos/go15035-vic/vblog/apps"
	"gitlab.com/vionysos/go15035-vic/vblog/apps/blog"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
	"gitlab.com/vionysos/go15035-vic/vblog/test"
)

var (
	// 声明被测试的对象
	serviceImpl blog.Service
	ctx         = context.Background()
)

// 被测试对象
func init() {
	test.DevelopmentSetup()
	serviceImpl = ioc.Controller.Get(blog.AppName).(blog.Service)
}
