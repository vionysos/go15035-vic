package impl

import (
	"gitlab.com/vionysos/go15035-vic/vblog/conf"
	"gitlab.com/vionysos/go15035-vic/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller.Registry(blog.AppName, &BlogServiceImpl{})

}

type BlogServiceImpl struct {
	// 每个业务对象，都可能依赖到数据库
	// db = create conn
	// 全局的MySQL
	db *gorm.DB
}

func (i *BlogServiceImpl) Init() error {
	// 获取对象 Controller.Get()
	i.db = conf.C().MySQL.GetDB()
	return nil
}
